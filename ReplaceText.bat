@echo off
setlocal enabledelayedexpansion
rem 检查是否提供了足够的参数
if "%~1"=="" (
    echo Please provide the first searchStr parameter
    exit /b 1
)
if "%~2"=="" (
    echo Please provide the second replaceStr parameter
    exit /b 1
)
if "%~3"=="" (
    echo Please provide the third sourceFilePath parameter
    exit /b 1
)



set "search=%~1"
set "replace=%~2"
set "sourceFilePath=%~3"

rem set "search=extensions = []"
rem set "replace=extensions = [new_value]"
rem set "replace=extensions = ['sphinx.ext.autodoc','sphinx.ext.napoleon','sphinx.ext.doctest','sphinx.ext.intersphinx','sphinx.ext.todo','sphinx.ext.coverage','sphinx.ext.mathjax',]"

rem set "sourceFilePath=D:\DocTemp\test\doc\source\conf.py"

for %%F in ("%sourceFilePath%") do (
    set "folder=%%~dpF"
	set "fileName=%%~nxF"
)

rem echo %sourceFilePath%
rem echo %search%
rem echo %replace%
rem exit /b 1

(for /f "delims=" %%a in (%sourceFilePath%) do (
    set "line=%%a"
    if "!line!" equ "%search%" (
        set "line=!replace!"
    )
    echo !line!
)) > "%folder%temp%fileName%"

move /y "%folder%temp%fileName%" %sourceFilePath%

echo relplace finished
endlocal
