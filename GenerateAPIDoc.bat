@echo off

rem 检查是否提供了足够的参数
if "%~1"=="" (
    echo Please provide the first productName parameter
    exit /b 1
)
if "%~2"=="" (
    echo Please provide the second productVer parameter
    exit /b 1
)
if "%~3"=="" (
    echo Please provide the third sourceCodePath parameter
    exit /b 1
)
if "%~4"=="" (
    echo Please provide the fourth targetPath parameter
    exit /b 1
)



rem 从参数中获取产品名称，产品版本，源代码路径和目标项目路径
set "productName=%~1"
set "productVer=%~2"
set "sourceCodePath=%~3"
set "targetPath=%~4"


if not exist "%sourceCodePath%" (
     echo  The sourceCodePath"%sourceCodePath%" is not exist
     exit /b 1
)

rem 拼接配置文件路径
set "confFile=%targetPath%\doc\source\conf.py"
rem 拼接暂存配置文件路径
set "tempConfFile=%targetPath%\doc\source\tempConf.py"

rem 拼接build路径
set "bulidPath=%targetPath%\doc\build"
rem 拼接source路径
set "sourcePath=%targetPath%\doc\source"

if not exist "%targetPath%" (

    rem 创建sphinx目录
    mkdir "%targetPath%"
    mkdir "%targetPath%"\doc
	
	rem 进入doc目录
	cd "%targetPath%\doc"
	rem 执行sphinx项目构建命令
	sphinx-quickstart --sep -p="%productName%" -a=e-iceblue -r="%productVer%" -l=en
	rem sphinx-quickstart --sep
	
	if not exist "%tempConfFile%" (
	    echo. > "%tempConfFile%"
		echo %tempConfFile% is create.
	)
	
	rem 将要添加的文本加入暂存配置文件
	(
	echo import os
	echo import sys
	echo sys.path.insert^(0,os.path.abspath^('%sourceCodePath%'^)^)
	) > "%tempConfFile%"
	
	rem 将源文件的内容追加到临时文件后面
	type %confFile% >> "%tempConfFile%"
	
	
	
	rem echo  %confFile% is not exist
	rem echo  %tempConfFile% is not exist
    rem exit /b 1
	
	
	set "s1=extensions = []"
	set "s2=html_theme = 'alabaster'"
    set "p1=extensions = ['sphinx.ext.autodoc','sphinx.ext.autosummary','sphinxcontrib.blockdiag','sphinx.ext.napoleon','sphinx.ext.doctest','sphinx.ext.intersphinx','sphinx.ext.todo','sphinx.ext.coverage','sphinx.ext.mathjax',]"
	set "p2=html_theme = 'sphinx_rtd_theme'"
	
	rem 获取当前脚本所在的路径
    rem for %%I in (%O) do (
    set "scriptPath=%~dp0"
    rem )

	setlocal enabledelayedexpansion
	rem 调用脚本修改tempConf.py配置文件
    call "!scriptPath!ReplaceText.bat" "!s1!" "!p1!" "!tempConfFile!"
    rem echo  !scriptPath!--------------
    rem exit /b 1
	call "!scriptPath!ReplaceText.bat" "!s2!" "!p2!" "!tempConfFile!"
	rem 修改tempConf.py配置文件结束
	endlocal
	
	
	rem 将暂存配置文件（tempConf.py）覆盖到实际使用的配置文件（conf.py）中
	move /y %tempConfFile% %confFile%
	
	rem 在doc目录使用sphinx执行生成API文档命令
	sphinx-apidoc -o source %sourceCodePath%
	
	rem 生成api文档
	.\make html
) else (
    rem 进入doc目录
	cd %targetPath%\doc
    
	
	echo %bulidPath%--------------
	echo %sourcePath%--------------
	
	rem 删除build目录下所有文件
	if exist "%bulidPath%" (
    for /d %%d in ("%bulidPath%\*") do (
        rd /s /q "%%d"
    )
    
    for %%f in ("%bulidPath%\*") do (
        del /q "%%f"
    )
    echo Folder contents deleted successfully.
    ) else (
    echo Folder not found.
    )
	
	rem 删除source目录下所有的rst文件，除了index.rst
	for %%F in ("%sourcePath%\*.rst") do (
	
	    if /i not "%%~nxF"=="index.rst" (
		    del "%%F"
			rem echo Deleted:"%%F"
		)
	
	)
	
	
	
	rem 在doc目录使用sphinx执行生成API文档命令
	sphinx-apidoc -o source %sourceCodePath%
	
	rem 生成api文档
	.\make html
	
)
rem 输出参数值（可选）
rem echo productName:%productName%

rem 在这里执行你的操作，例如复制文件到目标项目路径
rem xcopy "%productName%" "%targetPath%"

echo Genarate APIDOc Done!!
exit /b 0