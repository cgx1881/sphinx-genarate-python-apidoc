@echo off
setlocal enabledelayedexpansion

set "folderPath=D:\bat����\test"

if exist "%folderPath%" (
    for /d %%d in ("%folderPath%\*") do (
        rd /s /q "%%d"
    )
    
    for %%f in ("%folderPath%\*") do (
        del /q "%%f"
    )
    echo Folder contents deleted successfully.
) else (
    echo Folder not found.
)

endlocal



